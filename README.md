# Kalkulator

## A. Requirements
- Adobe Flash

## B. Final

![](code/final.png)

## C. Actionscript 3.0

### 1. Buat Layer Action, drag kebawah, klik kanan lalu klik Action.

![](code/Action.png)

### 2. Import?
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/0.png" width="60%">

### 3. Hubungan AC / All Clear (Button) dengan MouseEvent.CLICK dengan Function yang belum dibuat.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/1.png" width="60%">

### 4. Hubungkan Semua Button dengan Function.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/2.png" width="60%">

### 5. Merubah selectable menjadi False.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/3.png" width="60%">

### 6. Mendeklarasi dan menginisialisasi variable utama.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/4.png" width="70%">

### 7. Contoh membuat function untuk Clear All.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/5.png" width="70%">

### 8. Contoh membuat function untuk Delete.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/6.png" width="70%">

### 9. Membuat function satuClick, duaClick, dst.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/7.png" width="70%">

### 10. Membuat function setiap operator.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/8.png" width="70%">

### 11. Membuat function sdClick yang berarti Sama dengan.
    
<img src="https://gitlab.com/suganda8/kalkulator/-/raw/master/code/9.png" width="70%">